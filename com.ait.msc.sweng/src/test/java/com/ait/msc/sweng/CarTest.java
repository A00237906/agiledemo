package com.ait.msc.sweng;

import static org.junit.Assert.*;

import org.junit.*;
public class CarTest{
    
    Car car = new Car("VW", "Golf", 10000.00, 2010);
    
    @Test
    public void verifyMake(){
        assertEquals(car.getMake(), "VW");
    }

    @Test
    public void verifyModel(){
        assertEquals(car.getModel(), "Golf");
    }
    
    @Test
    public void verifyYear(){
        assertEquals(car.getYear(), 2010);
    }

}
